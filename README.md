# jobseek

This is a simple example of a job search portal, built with Lumen PHP framework for REST API generation, and &lt;TBD&gt; on the front-end.

All the city/state/employer names are fictitious. They have either been randomly generated using online tools, or carry influence of pop culture.
