<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return json_encode(['PoweredBy' => $router->app->version()]);
});

$router->get('employers', 'JobController@getEmployers');
$router->get('employer/profile/{id}', 'JobController@getEmployerProfile');

$router->get('job/{id}', 'JobController@getJobDetails');

$router->get('cities', 'JobController@getCities');
$router->get('location/suggestions/{keyword}', 'JobController@getLocationSuggestions');
$router->get('search', 'JobController@getSearchResults');

$router->get('user/{id}', function ($id) {
    return 'User ' . $id;
});
