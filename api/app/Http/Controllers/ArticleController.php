<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    public function getAll(Request $request)
    {
        $ids = $request->input('ids');
        $content = DB::select("Select concat(p.projectcode, '-', t.workitemkey) `Key`, t.TaskName Title, e.fullname `ReportedBy` from task t 
                                join project p on t.projectid = p.projectid
                                join employee e on e.employeeid = t.createdby
                                where t.workitemkey in ($ids)
                                order by p.projectcode, t.workitemkey");
        return response()->json($content, 200);
        // return response()->json(['error' => 'Unauthorized'], 401, ['X-Header-One' => 'Header Value']);
    }

    public function postOne(Request $request)
    {
        $content = $request->all();

        return response()->json($content, 200);
        // return response()->json(['error' => 'Unauthorized'], 401, ['X-Header-One' => 'Header Value']);
    }
}
