<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
    public function getEmployers(Request $request)
    {
        $limit = $request->has('limit') ? "LIMIT " . $request->input('limit') : "";

        $content = DB::select("SELECT e.*, c.*, s.*, GROUP_CONCAT(j.JobId) Jobs, COUNT(j.JobId) JobCount
                                FROM employer e
                                JOIN city c ON e.CityId = c.CityId
                                JOIN state s ON c.StateId = s.StateId
                                JOIN job j ON e.EmployerId = j.EmployerId
                                GROUP BY e.EmployerId, c.CityId
                                ORDER BY JobCount DESC, e.EmployerName $limit");


        $final = [
            'success' => true,
            'message' => null,
            'data' => $content
        ];
        return response()->json($final, 200);
    }

    public function getEmployerProfile($id)
    {
        $content = DB::select("SELECT *
                                FROM employer e
                                JOIN city c ON e.CityId = c.CityId
                                JOIN state s ON c.StateId = s.StateId AND e.EmployerId=?", [$id]);
        $employer = $content[0];
        $jobs = DB::select("select * from job where EmployerId=?", [$id]);
        $employer->Jobs = $jobs;
        $final = [
            'success' => true,
            'message' => null,
            'data' => $employer
        ];
        return response()->json($final, 200);
    }

    public function getJobDetails($id)
    {
        $content = DB::select("SELECT *
                                FROM job j
                                JOIN employer e ON e.EmployerId = j.EmployerId
                                JOIN city c ON e.CityId = c.CityId
                                JOIN state s ON c.StateId = s.StateId AND j.JobId=?", [$id]);
        $job = $content[0];
        $final = [
            'success' => true,
            'message' => null,
            'data' => $job
        ];
        return response()->json($final, 200);
    }

    public function getCities(Request $request)
    {
        $content = DB::select("SELECT *
                                FROM city c
                                JOIN state s ON c.StateId = s.StateId");

        $out = new \Symfony\Component\Console\Output\ConsoleOutput();

        // to be honest, this is probably not the best approach
        // because of how many queries are being run here
        // we should fetch all the data at once (by a join) and process them
        // would work for now, though
        foreach ($content as $c) {
            $r = DB::select("SELECT * from employer WHERE CityId=$c->CityId");
            $c->Employers = $r;
        }

        $final = [
            'success' => true,
            'message' => null,
            'data' => $content
        ];

        return response()->json($final, 200);
    }

    public function getLocationSuggestions($keyword)
    {
        $cities = DB::select("SELECT *
                                FROM city c
                                JOIN state s ON s.StateId = c.StateId
                                WHERE c.CityName LIKE '%$keyword%'");


        $states = DB::select("SELECT *
                                FROM state s
                                WHERE s.StateName LIKE '%$keyword%'
                                OR s.StateCode LIKE '%$keyword%'");

        if (!$keyword) {
            $cities = [];
            $states = [];
        }

        $final = [
            'success' => true,
            'message' => null,
            'data' => ['cities' => $cities, 'states' => $states]
        ];

        return response()->json($final, 200);
    }


    public function getSearchResults(Request $request)
    {
        $content = $request->all();
        $id_field = strtolower($content['lt'][0]) . "." . $content['lt'] . "Id";
        $id_value = $content['lid'];
        $text = $content['t'];
        $andClause = !empty($text) ? "AND j.JobDescription LIKE '%$text%'" : "";
        if (!$id_value || empty($id_value) || $id_value == 'undefined') {
            $id_field = 1;
            $id_value = 1;
        }

        $query = "SELECT j.*, e.EmployerName, c.CityName, s.StateName, s.StateCode
                    FROM job j
                    JOIN employer e ON j.EmployerId = e.EmployerId
                    JOIN city c ON j.CityId = c.CityId
                    JOIN state s ON c.StateId = s.StateId
                    WHERE $id_field = '$id_value' $andClause";

        $jobs = DB::select($query);

        $final = [
            'success' => true,
            'message' => null,
            'data' => $jobs
        ];

        return response()->json($final, 200);
    }

    /*

    The following JS code generates the list of jobs, stored here for convenience.
    Job description from https://resources.workable.com/software-developer-job-description

    
    let fullStr = "";
    for (i = 1; i <= 150; i++) {
    let ranks = ["", "Lead ", "Senior ", "Junior ", "Entry-Level "];
    let actions = [
        "",
        "Workflow ",
        "UI ",
        "Database ",
        "API ",
        "Front-End ",
        "Back-End ",
        "Mobile ",
        "Production Support "
    ];
    let roles = ["Developer", "Engineer", "Administrator", "Manager"];

    let rank = ranks[Math.floor(Math.random() * ranks.length)];
    let action = actions[Math.floor(Math.random() * actions.length)];
    let role = roles[Math.floor(Math.random() * roles.length)];

    let title = `${rank}${action}${role}`;
    let article = ["a", "e", "i", "o", "u"].includes(title.toLowerCase()[0])
        ? "an"
        : "a";
    let desc = `<p>We are looking for ${article} ${title} to join our diverse team. You will work with other Developers and Product Managers throughout the software development life cycle.</p><p>In this role, you should be a team player with a keen eye for detail and problem-solving skills. If you also have experience in Agile frameworks and popular coding languages, we’d like to meet you.</p><p>Your goal will be to build efficient programs and systems that serve user needs.</p><h2>Responsibilities</h2><ul><li>Work with developers to design algorithms and flowcharts</li><li>Produce clean, efficient code based on specifications</li><li>Integrate software components and third-party programs</li><li>Verify and deploy programs and systems</li><li>Troubleshoot, debug and upgrade existing software</li><li>Gather and evaluate user feedback</li><li>Recommend and execute improvements</li><li>Create technical documentation for reference and reporting</li></ul><h2>Preferred Qualifications</h2><ul><li>Proven experience as a Software Developer, Software Engineer or similar role</li><li>Familiarity with Agile development methodologies</li><li>Experience with software design and development in a test-driven environment</li><li>Knowledge of coding languages (e.g. C++, Java, JavaScript) and frameworks/systems (e.g. AngularJS, Git)</li><li>Experience with databases and Object-Relational Mapping (ORM) frameworks (e.g. Hibernate)</li><li>Ability to learn new languages and technologies</li><li>Excellent communication skills</li><li>Resourcefulness and troubleshooting aptitude</li><li>Attention to detail</li><li>BSc/BA in Computer Science, Engineering or a related field</li></ul>`;
    let employer = Math.round(Math.random() * 60);
    if (employer == 0) employer = 1;

    let reqd = Math.round(Math.random() * 10) + 3;
    let pref = reqd + Math.round(Math.random() * 3) + 1;

    let str = `INSERT INTO job (JobId, JobTitle, JobDescription, EmployerId, RequiredExperience, PreferredExperience) VALUES(${i}, '${title}', '${desc}', ${employer}, ${reqd}, ${pref});\n\n`;

    fullStr += str;
    }


    */
}
